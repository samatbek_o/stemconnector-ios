//
//  Util.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/7/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation

class Util {
    
    public static func validateString(_ txt: String) -> Bool {
        
        if (!txt.isEmpty) {
            return true
        }
        
        return false
    }
    
    public static func isNumeric(_ k: String) -> Bool {
        
        if(validateString(k)) {
            return Int(k) != nil
        }
        
        return false
    }

}
