//
// Created by Samatbek Osmonov on 9/11/17.
// Copyright (c) 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation
import ObjectMapper
class Initiative: Mappable {


    var idInitiative: Int? = nil
    var name: String? = nil
    var desc: String? = nil
    var imageURLString: String? = nil
    var imageURL: URL? {
        return imageURLString?.toUrl()
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        idInitiative   <- map["idInitiative"]
        name           <- map["name"]
        desc           <- map["description"]
        imageURLString <- map["imageUrl"]
    }
}
