//
//  Event.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/11/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation
import ObjectMapper

class Event: Mappable {
    var idEvent: Int?
    var name: String?
    var desc: String?
    var isActive: Bool?
    var location: String?
    
    private var startDateTimeString: String?
    var startDateTime: Date? {
        return startDateTimeString?.toDate()
    }
    
    private var endDateTimeString: String?
    var endDateTime: Date? {
        return endDateTimeString?.toDate()
    }
    
    private var imageURLString: String? = nil
    var imageURL: URL? {
        return imageURLString?.toUrl()
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        idEvent    <- map["idEvent"]
        name       <- map["name"]
        desc       <- map["description"]
        isActive   <- map["isActive"]
        location   <- map["location"]
        startDateTimeString  <- map["startDate"]
        endDateTimeString    <- map["endDate"]
        imageURLString <- map["imageUrl"]
    }
}
