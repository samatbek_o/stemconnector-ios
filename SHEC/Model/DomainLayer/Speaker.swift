//
//  Speaker.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/19/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation
import ObjectMapper

class Speaker: Mappable {
    var idSpeaker: Int?
    var name: String?
    var title: String?
    var bio: String?
    
    private var imageUrlString: String?
    var imageUrl: URL? {
        return imageUrlString?.toUrl()
    }

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        idSpeaker       <- map["idSpeaker"]
        name            <- map["name"]
        title           <- map["title"]
        bio             <- map["bio"]
        imageUrlString  <- map["imageUrl"]
    }
}
