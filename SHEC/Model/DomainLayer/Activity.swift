//
//  Activity.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/19/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation
import ObjectMapper

class Activity: Mappable {
    var idActivity: Int?
    var idEvent: Int?
    var name: String?
    var desc: String?
    
    private var startTimeString: String?
    var startTime: Date? {
        return startTimeString?.toDate()
    }
    
    private var endTimeString: String?
    var endTime: Date? {
        return startTimeString?.toDate()
    }
    
    var startAndEndTimeString: String? {
        guard let startString = self.startTime?.toTimeString(),
              let endString = self.endTime?.toTimeString() else {
            return nil
        }
        return startString + " - " + endString
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        idActivity      <- map["idActivity"]
        idEvent         <- map["Event_idEvent"]
        startTimeString <- map["startTime"]
        endTimeString   <- map["endTime"]
        name            <- map["name"]
        desc            <- map["description"]
    }
    
}
