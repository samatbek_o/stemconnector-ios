
//
//  EventDAO.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/11/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

class EventDAO: DAO {
    
    typealias GenericDataType = Event
    
    private init() {}
    
    static let sharedInstance = EventDAO()

    func read(_ key: AnyObject) -> Observable<Event> {
        return Observable.create({ (observer) -> Disposable in
            // TO DO. WE DON'T NEED THIS REQUEST YET.
            return Disposables.create()
        }).observeOn(MainScheduler.asyncInstance)
    }

    func readAll() -> Observable<[Event]> {
        return requestObservableDataArrayWithGet(with: Urls.events)
    }
    
    func readAll(byInitiativeId id: Int) -> Observable<[Event]> {
        let url = Urls.initiatives + "/\(id)/events"
        return requestObservableDataArrayWithGet(with: url)
    }
    
    func find(byName name: String) -> Observable<[Event]> {
        let parameters = ["name": name]
        return requestObservableDataArrayWithPost(with: Urls.eventsFind, parameters: parameters)
    }

    func create(_ c: Event) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }

    func update(_ key: Event) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }
    
    func delete(_ key: AnyObject) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }
}
