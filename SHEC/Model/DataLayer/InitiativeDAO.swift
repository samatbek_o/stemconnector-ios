//
// Created by Samatbek Osmonov on 9/11/17.
// Copyright (c) 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

class InitiativeDAO: DAO {

    private init() {}

    static let sharedInstance = InitiativeDAO()

    typealias GenericDataType = Initiative

    func read(_ key: AnyObject) -> Observable<Initiative> {
        return Observable.create({ (observer) -> Disposable in
            // TO DO. WE DON'T NEED THIS REQUEST YET.
            return Disposables.create()
        }).observeOn(MainScheduler.asyncInstance)
    }

    func readAll() -> Observable<[Initiative]> {
        return requestObservableDataArrayWithGet(with: Urls.initiatives)
    }

    func create(_ c: Initiative) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }

    func update(_ key: Initiative) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }

    func delete(_ key: AnyObject) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }

}
