//
//  DAO.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/7/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation

import RxSwift
import Alamofire
import ObjectMapper

protocol DAO {
    
    // Data Access Object (DAO) Protocol
    
    associatedtype GenericDataType
    
    func read(_ key: AnyObject) -> Observable<GenericDataType>
    
    func readAll() -> Observable<[GenericDataType]>

    func create(_ c : GenericDataType) -> Bool

    func update (_ key: GenericDataType) -> Bool
    
    func delete (_ key: AnyObject) -> Bool
    
}


// Extension for API Requests
extension DAO {
    
    func get(_ url: String,
             parameters: [String: Any]? = nil,
             completionHandler: @escaping (JSON)-> Void,
             errorHandler: @escaping (Error)-> Void) {
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            switch response.result {
            case .success:
                
                if let json = response.result.value as? JSON {
                    if (json["success"] as? Bool) == true {
                        completionHandler(json)
                    } else {
                        errorHandler(NSError(message: json["message"] as? String))
                    }
                }
                
            case .failure(let error):
                errorHandler(error)
            }
        }
    }
    
    func post(_ url: String,
              parameters: [String: Any]? = nil,
              completionHandler: @escaping (JSON)-> Void,
              errorHandler: @escaping (Error)-> Void) {
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default , headers: nil).responseJSON { (response) in
            
            switch response.result {
            case .success:
                
                if let json = response.result.value as? JSON {
                    if (json["success"] as? Bool) == true {
                        completionHandler(json)
                    } else {
                        errorHandler(NSError(message: json["message"] as? String))
                    }
                }
                
            case .failure(let error):
                errorHandler(error)
            }
        }
    }
}

// Extension for getting Observables
extension DAO {
    
    func requestObservableDataArrayWithGet<T: Mappable>(with url: String, parameters: [String : Any]? = nil) -> Observable<[T]>
        where T == GenericDataType {
            
        return Observable.create({ (observer) -> Disposable in
            self.get(url, parameters: parameters, completionHandler: { (json) in
                if let jsonData = json["data"] as? [JSON] {
                    let objects = Mapper<GenericDataType>().mapArray(JSONArray: jsonData)
                    observer.onNext(objects)
                }
                observer.onCompleted()
            }, errorHandler: { (error) in
                observer.onError(error)
            })
            return Disposables.create()
        }).observeOn(MainScheduler.asyncInstance)
    }
    
    func requestObservableDataArrayWithPost<T: Mappable>(with url: String, parameters: [String : Any]? = nil) -> Observable<[T]>
        where T == GenericDataType {
            
            return Observable.create({ (observer) -> Disposable in
                self.post(url, parameters: parameters, completionHandler: { (json) in
                    if let jsonData = json["data"] as? [JSON] {
                        let objects = Mapper<GenericDataType>().mapArray(JSONArray: jsonData)
                        observer.onNext(objects)
                    }
                    observer.onCompleted()
                }, errorHandler: { (error) in
                    observer.onError(error)
                })
                return Disposables.create()
            }).observeOn(MainScheduler.asyncInstance)
    }
    
}
