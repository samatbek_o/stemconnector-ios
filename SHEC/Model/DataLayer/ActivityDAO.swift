//
//  ActivityDAO.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/19/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation
import RxSwift

class ActivityDAO: DAO {
    
    private init() {}
    
    static let sharedInstance = ActivityDAO()
    
    typealias GenericDataType = Activity
    
    func read(_ key: AnyObject) -> Observable<Activity> {
        return Observable.create({ (observer) -> Disposable in
            // TO DO. WE DON'T NEED THIS REQUEST YET.
            return Disposables.create()
        }).observeOn(MainScheduler.asyncInstance)
    }
    
    func readAll() -> Observable<[Activity]> {
        return Observable.create({ (observer) -> Disposable in
            // TO DO. WE DON'T NEED THIS REQUEST YET.
            return Disposables.create()
        }).observeOn(MainScheduler.asyncInstance)
    }
    
    func readAll(byEvenId id: Int) -> Observable<[Activity]> {
        let url = Urls.events + "/\(id)" + Urls.activities
        return requestObservableDataArrayWithGet(with: url)
    }
    
    func create(_ c: Activity) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }
    
    func update(_ key: Activity) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }
    
    func delete(_ key: AnyObject) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }
}
