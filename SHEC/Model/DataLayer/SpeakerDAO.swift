//
//  SpeakerDAO.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/19/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation

import RxSwift

class SpeakerDAO: DAO {
    
    private init() {}
    
    static let sharedInstance = SpeakerDAO()
    
    typealias GenericDataType = Speaker
    
    func read(_ key: AnyObject) -> Observable<Speaker> {
        return Observable.create({ (observer) -> Disposable in
            // TO DO. WE DON'T NEED THIS REQUEST YET.
            return Disposables.create()
        }).observeOn(MainScheduler.asyncInstance)
    }
    
    func readAll() -> Observable<[Speaker]> {
        return Observable.create({ (observer) -> Disposable in
            // TO DO. WE DON'T NEED THIS REQUEST YET.
            return Disposables.create()
        }).observeOn(MainScheduler.asyncInstance)
    }
    
    func readAll(byEvenId id: Int) -> Observable<[Speaker]> {
        let url = Urls.events + "/\(id)" + Urls.speakers
        return requestObservableDataArrayWithGet(with: url)
    }
    
    func create(_ c: Speaker) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }
    
    func update(_ key: Speaker) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }
    
    func delete(_ key: AnyObject) -> Bool {
        // TO DO. WE DON'T NEED THIS REQUEST YET.
        return true
    }
}
