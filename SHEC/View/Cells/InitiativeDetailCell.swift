//
//  InitiativeDetailCell.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/13/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit
import SDWebImage

class InitiativeDetailCell: UITableViewCell {

    @IBOutlet weak var initiativeImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initiativeImageView.image = nil
    }
    
    func configureCell(with initiative: Initiative?) {
        guard let initiative = initiative else { return }
        
        nameLabel.text = initiative.name
        descriptionLabel.text = initiative.desc
        guard let url = initiative.imageURL else {return}
        initiativeImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
    }
}
