//
//  EventCell.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/13/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {
    
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        eventImageView.image = nil
    }
    
    func configureCell(with event: Event) {
        nameLabel.text = event.name
        guard let url = event.imageURL else {return}
        eventImageView.sd_setImage(with: url)
    }
}
