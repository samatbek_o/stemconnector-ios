//
//  EventCell.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 8/31/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit
import SDWebImage

class InitiativeListCell: UICollectionViewCell {
    @IBOutlet weak var initiativeImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initiativeImageView.layer.masksToBounds = true
        initiativeImageView.layer.cornerRadius = 5
        initiativeImageView.image = nil
    }
    
    func configureCell(with initiative: Initiative) {
        nameLabel.text = initiative.name
        
        guard let url = initiative.imageURL else {return}
        initiativeImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
    }
}
