//
//  SpeakerCell.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/5/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit
import SDWebImage

class SpeakerCell: UITableViewCell {

    @IBOutlet weak var speakerImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        speakerImageView.layer.cornerRadius = (speakerImageView.bounds.height / 2)
        speakerImageView.clipsToBounds = true
        speakerImageView.image = nil
    }
    
    func configureCell(with speaker: Speaker) {
        nameLabel.text = speaker.name
        titleLabel.text = speaker.title
        guard let url = speaker.imageUrl else { return }
        speakerImageView.sd_setImage(with: url)
    }
    
}
