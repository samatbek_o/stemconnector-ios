//
//  AgendaCell.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/5/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit

class AgendaCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(with activity: Activity) {
        nameLabel.text = activity.name
        timeLabel.text = activity.startAndEndTimeString
        descriptionLabel.text = activity.desc
    }
    
}
