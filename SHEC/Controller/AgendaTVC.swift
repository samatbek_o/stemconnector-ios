//
//  AgendaTVC.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/5/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit

class AgendaTVC: BaseTVC {

    var eventId: Int?
    var activities: [Activity] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 20
        
        loadData()
    }
    
    func loadData() {
        activityIndicatorView.startAnimating()
        guard let id = eventId else { return }
        ActivityDAO.sharedInstance.readAll(byEvenId: id)
            .subscribe(onNext: { (activities) in
                self.activities = activities
            }, onError: { (error) in
                self.activityIndicatorView.stopAnimating()
                self.showAlertView(message: error.localizedDescription)
            }, onCompleted: {
                self.activityIndicatorView.stopAnimating()
            }).addDisposableTo(self.disposeBag)
    }
    
    
    func registerCells() {
        tableView.register(AgendaCell.self, forCellReuseIdentifier: Constants.AGENDA_CELL_ID)
        tableView.register(UINib(nibName:Constants.AGENDA_CELL_ID, bundle: nil), forCellReuseIdentifier: Constants.AGENDA_CELL_ID)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return activities.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.AGENDA_CELL_ID, for: indexPath) as! AgendaCell
        cell.configureCell(with: activities[indexPath.row])
        return cell
    }
}
