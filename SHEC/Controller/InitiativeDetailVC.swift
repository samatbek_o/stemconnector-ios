//
//  InitiativeVC.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/4/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit

class InitiativeDetailVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    
    var initiative: Initiative?
    var events = [Event]() {
        didSet {tableView.reloadData()}
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.gray
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        tableView.addSubview(self.refreshControl)
        
        loadData()
    }
    
    func loadData() {
        if let id = initiative?.idInitiative {
//            activityIndicatorView.startAnimating()
            EventDAO.sharedInstance.readAll(byInitiativeId: id)
                .subscribe(onNext: { (events) in
                    self.events = events
                }, onError: { (error) in
                    self.activityIndicatorView.stopAnimating()
                    self.showAlertView(message: error.localizedDescription)
                }, onCompleted: {
                    self.activityIndicatorView.stopAnimating()
                }).addDisposableTo(self.disposeBag)
        }

    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        guard let id = initiative?.idInitiative else {
            refreshControl.endRefreshing()
            return
        }
        
        EventDAO.sharedInstance.readAll(byInitiativeId: id)
            .subscribe(onNext: { (events) in
                self.events = events
                self.refreshControl.endRefreshing()
            }, onError: { (error) in
                self.refreshControl.endRefreshing()
                self.showAlertView(message: error.localizedDescription)
            }, onCompleted: {
                self.refreshControl.endRefreshing()
            }).addDisposableTo(self.disposeBag)
    }
    
    func registerCells() {
        tableView.register(EventCell.self, forCellReuseIdentifier: Constants.EVENT_CELL_ID)
        tableView.register(UINib(nibName:Constants.EVENT_CELL_ID, bundle: nil), forCellReuseIdentifier: Constants.EVENT_CELL_ID)
        
        
        tableView.register(InitiativeDetailCell.self, forCellReuseIdentifier: Constants.INITIATIVE_DETAIL_CELL_ID)
        tableView.register(UINib(nibName:Constants.INITIATIVE_DETAIL_CELL_ID, bundle: nil), forCellReuseIdentifier: Constants.INITIATIVE_DETAIL_CELL_ID)
    }
}

extension InitiativeDetailVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return events.count == 0 ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let initiativeCell = tableView.dequeueReusableCell(withIdentifier: Constants.INITIATIVE_DETAIL_CELL_ID, for: indexPath) as! InitiativeDetailCell
            initiativeCell.configureCell(with: initiative)
            return initiativeCell
        }
        
        let eventCell = tableView.dequeueReusableCell(withIdentifier: Constants.EVENT_CELL_ID, for: indexPath) as! EventCell
        eventCell.configureCell(with: events[indexPath.row])
        return eventCell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? nil : "Events"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else {return}
        
        header.backgroundView?.backgroundColor = Constants.PURPLE_COLOR
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = UIFont(name: "WorkSans-SemiBold", size: 18)
        header.textLabel?.textAlignment = .center
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 { return }
        goToEventDetailTVC(withEventIndex: indexPath.row)
    }
    
    
    func goToEventDetailTVC(withEventIndex index: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailTVC") as? EventDetailTVC else { return }
        vc.event = events[index]
        navigationController?.pushViewController(vc,
                                                 animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let sectionHeaderHeight: CGFloat = 40
        if scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y >= 0 {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0)
        }
        else if scrollView.contentOffset.y >= sectionHeaderHeight {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
        }
        return 80
    }
}
