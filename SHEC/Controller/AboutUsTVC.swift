//
//  AboutUsVC.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/4/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit

class AboutUsTVC: BaseTVC {
    @IBOutlet weak var desctiptionLabel: UILabel!
    
    @IBOutlet weak var websiteTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 40
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
   
    @IBAction func facebookButtonTapped(_ sender: UIButton) {
        openUrlInSafari(url: Urls.facebook)
    }
    
    @IBAction func twitterButtonTapped(_ sender: UIButton) {
        openUrlInSafari(url: Urls.twitter)
    }
    
    @IBAction func googleButtonTapped(_ sender: UIButton) {
        openUrlInSafari(url: Urls.google)
    }
    
    @IBAction func linkedinButtonTapped(_ sender: UIButton) {
        openUrlInSafari(url: Urls.linkedin)
    }
    
    func openUrlInSafari(url: URL?) {
        guard let url = url else {
            return
        }
        UIApplication.shared.openURL(url)
    }
}
