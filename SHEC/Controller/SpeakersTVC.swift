//
//  SpeakersTVC.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/5/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit

class SpeakersTVC: BaseTVC {
    
    var eventId: Int?
    var speakers: [Speaker] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        loadData()
    }
    
    func loadData() {
        guard let id = eventId else { return }
        activityIndicatorView.startAnimating()
        SpeakerDAO.sharedInstance.readAll(byEvenId: id)
            .subscribe(onNext: { (speakers) in
                self.speakers = speakers
            }, onError: { (error) in
                self.activityIndicatorView.stopAnimating()
                self.showAlertView(message: error.localizedDescription)
            }, onCompleted: {
                self.activityIndicatorView.stopAnimating()
            }).addDisposableTo(self.disposeBag)
    }
    
    func registerCells() {
        tableView.register(SpeakerCell.self, forCellReuseIdentifier: Constants.SPEAKER_CELL_ID)
        tableView.register(UINib(nibName:Constants.SPEAKER_CELL_ID, bundle: nil), forCellReuseIdentifier: Constants.SPEAKER_CELL_ID)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return speakers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.SPEAKER_CELL_ID, for: indexPath) as! SpeakerCell
        cell.configureCell(with: speakers[indexPath.row])
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
