//
//  EventTVC.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/5/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit
import RxSwift
import SDWebImage

class EventDetailTVC: BaseTVC {
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var datetimeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var event: Event?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 40
        eventImageView.image = nil
        setupUI()
    }
    
    func setupUI() {
        guard let event = event else { return }
    
        nameLabel.text = event.name
        datetimeLabel.text = event.startDateTime?.toStringWithLongFormat()
        addressLabel.text = event.location
        descriptionLabel.text = event.desc
        
        guard let url = event.imageURL else { return }
        eventImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
    }

    @IBAction func agendaButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "AgendaTVC") as? AgendaTVC else { return }
        vc.eventId = event?.idEvent
        navigationController?.pushViewController(vc,
                                                 animated: true)
    }
    
    @IBAction func spreakersButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "SpeakersTVC") as? SpeakersTVC else { return }
        vc.eventId = event?.idEvent
        navigationController?.pushViewController(vc,
                                                 animated: true)
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
