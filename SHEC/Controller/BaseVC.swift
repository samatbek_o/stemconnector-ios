//
//  BaseVC.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/4/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit
import RxSwift

class BaseVC: UIViewController {
    
    let disposeBag = DisposeBag()
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicatorView.hidesWhenStopped = true
        
        var center = self.view.center
        if let navigationBarFrame = self.navigationController?.navigationBar.frame {
            center.y -= (navigationBarFrame.origin.y + navigationBarFrame.size.height)
        }
        
        activityIndicatorView.center = center
        self.view.addSubview(activityIndicatorView)
        return activityIndicatorView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        let titleAttributes = [
            NSFontAttributeName: UIFont(name: "WorkSans-SemiBold", size: 17)!,
            NSForegroundColorAttributeName: UIColor.white
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = titleAttributes
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func showAlertView(title: String? = "Something failed", message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        alert.view.tintColor = Constants.PURPLE_COLOR
        self.present(alert, animated: true, completion: nil)
    }
}
