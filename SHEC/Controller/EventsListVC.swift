//
//  EventsVC.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/14/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit
import RxSwift

class EventsListVC: BaseVC {
    
    var events = [Event]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.gray
        
        return refreshControl
    }()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    @IBOutlet weak var searchBarTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        tableView.addSubview(self.refreshControl)
        registerCells()
        
        loadData()
    }
    
    func loadData() {
        activityIndicatorView.startAnimating()
        EventDAO.sharedInstance.readAll()
            .subscribe(onNext: { (events) in
                self.events = events
            }, onError: { (error) in
                self.activityIndicatorView.stopAnimating()
                self.showAlertView(message: error.localizedDescription)
            }, onCompleted: {
                self.activityIndicatorView.stopAnimating()
            }).addDisposableTo(self.disposeBag)
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        EventDAO.sharedInstance.readAll()
            .subscribe(onNext: { (events) in
                self.events = events
                self.refreshControl.endRefreshing()
            }, onError: { (error) in
                self.refreshControl.endRefreshing()
                self.showAlertView(message: error.localizedDescription)
            }, onCompleted: {
                self.refreshControl.endRefreshing()
            }).addDisposableTo(self.disposeBag)
    }
    
    func registerCells() {
        tableView.register(EventCell.self, forCellReuseIdentifier: Constants.EVENT_CELL_ID)
        tableView.register(UINib(nibName:Constants.EVENT_CELL_ID, bundle: nil), forCellReuseIdentifier: Constants.EVENT_CELL_ID)
    }
}

extension EventsListVC: UISearchBarDelegate {
    
    @IBAction func searchButtonTapped(_ sender: UIBarButtonItem) {
        searchBar.becomeFirstResponder()
        searchBar.setShowsCancelButton(true, animated: true)
        searchBarTopConstraint.constant = 0
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.searchBar.text = ""
        self.searchBarTopConstraint.constant = -44
        self.searchBar.endEditing(true)
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
        loadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        activityIndicatorView.startAnimating()
        EventDAO.sharedInstance.find(byName: searchText)
            .subscribe(onNext: { (events) in
                self.events = events
            }, onError: { (error) in
                self.activityIndicatorView.stopAnimating()
                self.showAlertView(message: error.localizedDescription)
            }, onCompleted: { 
                self.activityIndicatorView.stopAnimating()
            }).addDisposableTo(self.disposeBag)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

extension EventsListVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.EVENT_CELL_ID, for: indexPath) as! EventCell
        cell.configureCell(with: events[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        goToEventDetailTVC(withEventIndex: indexPath.row)
    }
    
    
    func goToEventDetailTVC(withEventIndex index: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailTVC") as? EventDetailTVC else { return }
        vc.event = events[index]
        
        searchBar.endEditing(true)
        navigationController?.pushViewController(vc,
                                                 animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchBar.endEditing(true)
    }
}
