//
//  MainVC.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 8/31/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit
import RxSwift

class MainVC: BaseVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var initiatives = [Initiative]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.gray
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        collectionView.alwaysBounceVertical = true
        collectionView.addSubview(refreshControl)
        
        loadData()
    }
    
    func loadData() {
        activityIndicatorView.startAnimating()
        InitiativeDAO.sharedInstance.readAll()
            .subscribe(onNext: { (initiatives) in
                self.initiatives = initiatives
            }, onError: { (error) in
                self.activityIndicatorView.stopAnimating()
                self.showAlertView(message: error.localizedDescription)
            }, onCompleted: {
                self.activityIndicatorView.stopAnimating()
            }).addDisposableTo(self.disposeBag)
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        InitiativeDAO.sharedInstance.readAll()
            .subscribe(onNext: { (initiatives) in
                self.initiatives = initiatives
                refreshControl.endRefreshing()
            }, onError: { (error) in
                refreshControl.endRefreshing()
                self.showAlertView(message: error.localizedDescription)
            }, onCompleted: { 
                refreshControl.endRefreshing()
            }).addDisposableTo(self.disposeBag)
        
    }
    
    func registerCells() {
        collectionView.register(InitiativeListCell.self, forCellWithReuseIdentifier:Constants.INITIATIVE_LIST_CELL_ID)
    
        collectionView.register(UINib(nibName:Constants.INITIATIVE_LIST_CELL_ID, bundle: nil), forCellWithReuseIdentifier:Constants.INITIATIVE_LIST_CELL_ID)
    }
}

extension MainVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return initiatives.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.INITIATIVE_LIST_CELL_ID, for: indexPath) as! InitiativeListCell
        cell.configureCell(with: initiatives[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        goToInitiativeVC(indexPath: indexPath)
    }
    
    func goToInitiativeVC(indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "InitiativeDetailVC") as? InitiativeDetailVC else { return }
        vc.initiative = initiatives[indexPath.row]
        navigationController?.pushViewController(vc,
                                                 animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
}



























