//
//  Constants.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 8/31/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import UIKit

struct Constants {
    static let INITIATIVE_LIST_CELL_ID = "InitiativeListCell"
    static let INITIATIVE_CELL_ID = "InitiativeCell"
    static let EVENT_HEADER_CELL_ID = "EventHeaderCell"
    static let EVENT_CELL_ID = "EventCell"
    static let INITIATIVE_DETAIL_CELL_ID = "InitiativeDetailCell"
    static let SPEAKER_CELL_ID = "SpeakerCell"
    static let AGENDA_CELL_ID = "AgendaCell"
    
    static let PURPLE_COLOR = UIColor(red:0.25, green:0.25, blue:0.60, alpha:1.0)
    static let GRAY_COLOR = UIColor(red:0.31, green:0.31, blue:0.31, alpha:1.0)
    
//    static let PURPLE_COLOR = UIColor(red:0.26, green:0.28, blue:0.56, alpha:1.0)
    
}
