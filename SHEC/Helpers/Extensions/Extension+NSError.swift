//
//  Extension+NSError.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/19/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation

extension NSError {
    
    convenience init(message:String?) {
        var errorMessage = NSLocalizedString("UNKNOWN_ERROR", comment: "")
        
        if (message != nil) {
            errorMessage = message!
        }
        
        let userInfo: [NSObject : AnyObject] =
            [
                NSLocalizedDescriptionKey as NSObject : errorMessage as AnyObject
        ]
        
        self.init(domain: "",code: 200, userInfo: userInfo)
    }
}
