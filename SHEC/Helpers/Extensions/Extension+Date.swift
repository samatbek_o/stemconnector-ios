//
//  Extension+Date.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/19/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation

extension Date {
    func toStringWithLongFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        dateFormatter.dateFormat = "eeee, MMMM d yyyy\nh a"
        
        return dateFormatter.string(from: self)
    }
    
    func toTimeString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h a"
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        
        return dateFormatter.string(from: self)
    }
}
