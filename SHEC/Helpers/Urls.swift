//
//  Paths.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/11/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation

class Urls {
    
    private static let api = "http://165.227.179.52:3000"
//    private static let api = "http://Samatbeks-MacBook-Pro.local:3000"
//    private static let api = "http://localhost:3000"
    
    
    static let events = api + "/events"
    static let eventsFind = events + "/find"
    static let initiatives = api + "/initiatives"
    static let activities = "/activities"
    static let speakers = "/speakers"
    
    static let facebook = URL(string: "http://www.facebook.com/STEMconnector")
    static let twitter = URL(string: "http://www.twitter.com/STEMconnector")
    static let google = URL(string: "https://plus.google.com/110561112402950457829/posts")
    static let linkedin = URL(string: "https://www.linkedin.com/company/10042128")
}
