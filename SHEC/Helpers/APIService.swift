//
//  APIService.swift
//  SHEC
//
//  Created by Samatbek Osmonov on 9/11/17.
//  Copyright © 2017 Samatbek Osmonov. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import ObjectMapper

typealias JSON = [String:Any]

class APIService {
    
    private static let _instance = APIService()
    static var Instance: APIService {
        return _instance
    }
    
    func get(api: String, parameters: [String: Any]? = nil, completionHandler: @escaping (JSON?)-> Void, errorHandler: @escaping (Error)-> Void) {
        
        Alamofire.request(api, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            switch response.result {
            case .success:
                let json = response.result.value as? JSON
                
                if let success = json?["success"] as? Bool, success == true  {
                    completionHandler(json)
                } else {
                    errorHandler(NSError(message: json?["message"] as? String))
                }
            case .failure(let error):
                errorHandler(error)
            }
        }
    }
    
    func post(api: String, parameters: [String: Any]? = nil, completionHandler: @escaping (JSON?)-> Void, errorHandler: @escaping (Error)-> Void) {
        
        Alamofire.request(api, method: .post, parameters: parameters, encoding: JSONEncoding.default , headers: nil).responseJSON { (response) in
            
            switch response.result {
            case .success:
                let json = response.result.value as? JSON
                completionHandler(json)
            case .failure(let error):
                errorHandler(error)
            }
        }
    }
}
